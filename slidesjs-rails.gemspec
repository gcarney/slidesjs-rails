# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "slidesjs-rails"
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Daniel Libanori"]
  s.date = "2016-05-12"
  s.description = "Slidesjs assets for Rails 3.1"
  s.email = ["daniellibanori@gmail.com"]
  s.files = [".gitignore", "Gemfile", "LICENSE.txt", "README.md", "Rakefile", "lib/slidesjs/rails.rb", "lib/slidesjs/rails/engine.rb", "lib/slidesjs/rails/version.rb", "slidesjs-rails.gemspec", "vendor/assets/javascripts/jquery.slides.js"]
  s.homepage = "https://github.com/dlibanori/slidesjs-rails"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "2.0.14"
  s.summary = "Slidesjs assets for Rails 3.1"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>, ["~> 1.3"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_runtime_dependency(%q<railties>, ["<= 4.0.5", ">= 3.1"])
    else
      s.add_dependency(%q<bundler>, ["~> 1.3"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<railties>, ["<= 4.0.5", ">= 3.1"])
    end
  else
    s.add_dependency(%q<bundler>, ["~> 1.3"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<railties>, ["<= 4.0.5", ">= 3.1"])
  end
end
